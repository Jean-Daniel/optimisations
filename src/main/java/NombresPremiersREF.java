import java.util.ArrayList;
import java.util.List;

/**
 * Created by nico on 2016-11-15.
 */
public class NombresPremiersREF extends Thread
{
    int MAX;
    List<Integer> listePremiers;

    public NombresPremiersREF(int MAX)
    {
        this.MAX = MAX;
        listePremiers = new ArrayList<Integer>();
    }

    public void run()
    {
        long temps = calculePremiers();
        System.out.println(getClass().toString() + " " + listePremiers.size() + " nombres premiers trouvés en " + temps + " ms\n");
    }

    private boolean estPremier(int candidat)
    {
        boolean premier = true;
        for (int diviseur = 2; diviseur < candidat; diviseur++) {
            int resultat = candidat % diviseur;
            if (0 == resultat) {
                premier = false;
            }
        }
        return premier;
    }

    private long calculePremiers()
    {
        long debut = System.currentTimeMillis();
        for (int candidat=3; candidat < MAX; candidat++) {
            if (estPremier(candidat)) {
                listePremiers.add(candidat);
            }
        }
        return (System.currentTimeMillis() - debut);
    }

}
